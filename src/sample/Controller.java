package sample;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;

/**
 * Класс, демонстрирующий работу конвертера
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Tab length;

    @FXML
    private ComboBox<?> boxFromLength;

    @FXML
    private ComboBox<?> boxInLength;

    @FXML
    private TextField numberOneLength;

    @FXML
    private TextField numberTwoLength;

    @FXML
    private Label resultLength;

    @FXML
    private Button butttonLength;

    @FXML
    private Button buttonClearLength;

    @FXML
    private Tab weight;

    @FXML
    private ComboBox<?> boxFromWeight;

    @FXML
    private ComboBox<?> boxInWeight;

    @FXML
    private TextField numberOneWeight;

    @FXML
    private TextField numberTwoWeight;

    @FXML
    private Label resultWeight;

    @FXML
    private Button butttonWeight1;

    @FXML
    private Button buttonClearWeight;

    @FXML
    private Tab Time;

    @FXML
    private ComboBox<?> boxFromTime;

    @FXML
    private ComboBox<?> boxInTime;

    @FXML
    private TextField numberOneTime;

    @FXML
    private TextField numberTwoTime;

    @FXML
    private Label resultTime;

    @FXML
    private Button butttonTime;

    @FXML
    private Button buttonClearTime;

    /**
     * Метод, очищающий поле ввода длины и поле рузультата
     *
     * @param event Нажатие на кнопку "Очистить"
     */
    @FXML
    void clearLength(ActionEvent event) {
        numberOneLength.clear();
        numberTwoLength.clear();
    }

    /**
     * Метод, очищающий поле ввода времени и поле рузультата
     *
     * @param event Нажатие на кнопку "Очистить"
     */
    @FXML
    void clearTime(ActionEvent event) {
        numberOneTime.clear();
        numberTwoTime.clear();
    }

    /**
     * Метод, очищающий поле ввода массы и поле рузультата
     *
     * @param event Нажатие на кнопку "Очистить"
     */
    @FXML
    void clearWeight(ActionEvent event) {
        numberOneWeight.clear();
        numberTwoWeight.clear();
    }

    /**
     * Метод, для перевода единиц длины
     * <p>
     * Метод проверяет чему равен комбобокс из которого конвертируем
     * и в который конвертируем
     * <p>
     * В зависимости от значений комбобоксов выполняем тот или иной код
     *
     * @param event Нажатие на конпку "Перевести"
     */
    @FXML
    void translationLength(ActionEvent event) {
        double numberFirst;
        if (isValidation(numberOneLength) == true) {
            numberFirst = parses(numberOneLength);
            double result = 0;
            if (boxFromLength.getValue().equals("километр") && boxInLength.getValue().equals("метр")) {
                result = numberFirst * 1000;
            }
            if (boxFromLength.getValue().equals("километр") && boxInLength.getValue().equals("километр")) {
                result = numberFirst;
            }
            if (boxFromLength.getValue().equals("километр") && boxInLength.getValue().equals("сантиметр")) {
                result = numberFirst * 100000;
            }
            if (boxFromLength.getValue().equals("километр") && boxInLength.getValue().equals("дециметр")) {
                result = numberFirst * 10000;
            }
            if (boxFromLength.getValue().equals("километр") && boxInLength.getValue().equals("миллиметр")) {
                result = numberFirst * 1000000;
            }
            if (boxFromLength.getValue().equals("метр") && boxInLength.getValue().equals("метр")) {
                result = numberFirst;
            }
            if (boxFromLength.getValue().equals("метр") && boxInLength.getValue().equals("километр")) {
                result = numberFirst * 0.001;
            }
            if (boxFromLength.getValue().equals("метр") && boxInLength.getValue().equals("дециметр")) {
                result = numberFirst * 10;
            }
            if (boxFromLength.getValue().equals("метр") && boxInLength.getValue().equals("сантиметр")) {
                result = numberFirst * 100;
            }
            if (boxFromLength.getValue().equals("метр") && boxInLength.getValue().equals("миллиметр")) {
                result = numberFirst * 1000;
            }
            if (boxFromLength.getValue().equals("сантиметр") && boxInLength.getValue().equals("сантиметр")) {
                result = numberFirst;
            }
            if (boxFromLength.getValue().equals("сантиметр") && boxInLength.getValue().equals("километр")) {
                result = numberFirst * 0.00001;
            }
            if (boxFromLength.getValue().equals("сантиметр") && boxInLength.getValue().equals("метр")) {
                result = numberFirst * 0.01;
            }
            if (boxFromLength.getValue().equals("сантиметр") && boxInLength.getValue().equals("дециметр")) {
                result = numberFirst * 0.1;
            }
            if (boxFromLength.getValue().equals("сантиметр") && boxInLength.getValue().equals("миллиметр")) {
                result = numberFirst * 10;
            }
            if (boxFromLength.getValue().equals("дециметр") && boxInLength.getValue().equals("дециметр")) {
                result = numberFirst;
            }
            if (boxFromLength.getValue().equals("дециметр") && boxInLength.getValue().equals("километр")) {
                result = numberFirst * 0.0001;
            }
            if (boxFromLength.getValue().equals("дециметр") && boxInLength.getValue().equals("метр")) {
                result = numberFirst * 0.1;
            }
            if (boxFromLength.getValue().equals("дециметр") && boxInLength.getValue().equals("сантиметр")) {
                result = numberFirst * 10;
            }
            if (boxFromLength.getValue().equals("дециметр") && boxInLength.getValue().equals("миллиметр")) {
                result = numberFirst * 100;
            }
            if (boxFromLength.getValue().equals("миллиметр") && boxInLength.getValue().equals("миллиметр")) {
                result = numberFirst;
            }
            if (boxFromLength.getValue().equals("миллиметр") && boxInLength.getValue().equals("километр")) {
                result = numberFirst * 0.000001;
            }
            if (boxFromLength.getValue().equals("миллиметр") && boxInLength.getValue().equals("метр")) {
                result = numberFirst * 0.001;
            }
            if (boxFromLength.getValue().equals("миллиметр") && boxInLength.getValue().equals("дециметр")) {
                result = numberFirst * 0.01;
            }
            if (boxFromLength.getValue().equals("миллиметр") && boxInLength.getValue().equals("сантиметр")) {
                result = numberFirst * 0.1;
            }
            setText(result, numberTwoLength);
        } else {
            outPutError(numberTwoLength);
        }
    }

    /**
     * Метод, для перевода единиц времени
     * <p>
     * Метод проверяет чему равен комбобокс из которого конвертируем
     * и в который конвертируем
     * <p>
     * В зависимости от значений комбобоксов выполняем тот или иной код
     *
     * @param event нажатие на кнопку "Перевести"
     */
    @FXML
    void translationTime(ActionEvent event) {

        double numberFirst;
        if (isValidation(numberOneTime) == true) {
            numberFirst = parses(numberOneTime);
            double result = 0;
            if (boxFromTime.getValue().equals("секунда") && boxInTime.getValue().equals("секунда")) {
                result = numberFirst;
            }
            if (boxFromTime.getValue().equals("секунда") && boxInTime.getValue().equals("минута")) {
                result = numberFirst / 60;
            }
            if (boxFromTime.getValue().equals("секунда") && boxInTime.getValue().equals("час")) {
                result = numberFirst / 3600;
            }
            if (boxFromTime.getValue().equals("минута") && boxInTime.getValue().equals("минута")) {
                result = numberFirst;
            }
            if (boxFromTime.getValue().equals("минута") && boxInTime.getValue().equals("секунда")) {
                result = numberFirst * 60;
            }
            if (boxFromTime.getValue().equals("минута") && boxInTime.getValue().equals("час")) {
                result = numberFirst / 60;
            }
            if (boxFromTime.getValue().equals("час") && boxInTime.getValue().equals("час")) {
                result = numberFirst;
            }
            if (boxFromTime.getValue().equals("час") && boxInTime.getValue().equals("секунда")) {
                result = numberFirst * 3600;
            }
            if (boxFromTime.getValue().equals("час") && boxInTime.getValue().equals("минута")) {
                result = numberFirst * 60;
            }
            setText(result, numberTwoTime);
        } else {
            outPutError(numberTwoTime);

        }
    }

    /**
     * Метод, для перевода единиц массы.
     * <p>
     * Метод проверяет чему равен комбобокс из которого конвертируем
     * и в который конвертируем
     * <p>
     * В зависимости от значений комбобоксов выполняем тот или иной код
     *
     * @param event нажатие на кнопку "Перевести"
     */
    @FXML
    void translationWeight(ActionEvent event) {
        double numberFirst;
        if (isValidation(numberOneWeight) == true) {
            numberFirst = parses(numberOneWeight);
            double result = 0;
            if (boxFromWeight.getValue().equals("грамм") && boxInWeight.getValue().equals("грамм")) {
                result = numberFirst;
            }
            if (boxFromWeight.getValue().equals("грамм") && boxInWeight.getValue().equals("килограмм")) {
                result = numberFirst * 0.001;
            }
            if (boxFromWeight.getValue().equals("грамм") && boxInWeight.getValue().equals("тоннна")) {
                result = numberFirst * 0.000001;
            }
            if (boxFromWeight.getValue().equals("килограмм") && boxInWeight.getValue().equals("килограмм")) {
                result = numberFirst;
            }
            if (boxFromWeight.getValue().equals("килограмм") && boxInWeight.getValue().equals("грамм")) {
                result = numberFirst * 1000;
            }
            if (boxFromWeight.getValue().equals("килограмм") && boxInWeight.getValue().equals("тонна")) {
                result = numberFirst * 0.001;
            }
            if (boxFromWeight.getValue().equals("тонна") && boxInWeight.getValue().equals("тонна")) {
                result = numberFirst;
            }
            if (boxFromWeight.getValue().equals("тонна") && boxInWeight.getValue().equals("килограмм")) {
                result = numberFirst * 1000;
            }
            if (boxFromWeight.getValue().equals("тонна") && boxInWeight.getValue().equals("грамм")) {
                result = numberFirst * 1000000;
            }
            setText(result, numberTwoWeight);
        } else {
            outPutError(numberTwoWeight);

        }

    }

    @FXML
    void initialize() {

    }

    /**
     * Метод, выводящий сообщение, если введёное число некорректно
     * <p>
     * Цвет текста красный(использую CSS)
     *
     * @param numberTwo результат
     */
    private void outPutError(TextField numberTwo) {
        numberTwo.setText("Некорректное число");
        numberTwo.setStyle("-fx-text-fill: red;");
    }

    /**
     * Метод, проверяющий число на валидность(парсится или нет)
     *
     * @param number число вводимое пользователем
     * @return true - если парсится, false - если нет
     */
    boolean isValidation(TextField number) {
        try {
            Double.parseDouble(number.getText());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Метод, осуществляющий парсинг
     *
     * @param numberOne число вводимое пользователем
     * @return число типа double
     */
    double parses(TextField numberOne) {
        return Double.parseDouble(numberOne.getText());
    }

    /**
     * Метод, выводящий результат вычислений типа String
     * <p>
     * Цвет текста чёрный (использую CSS)
     *
     * @param number
     * @param numberTwo
     */
    void setText(double number, TextField numberTwo) {
        numberTwo.setText(String.valueOf(number));
        numberTwo.setStyle("-fx-text-fill: black;");
    }
}
